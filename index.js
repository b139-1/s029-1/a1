const express = require("express");
const app = express();
const PORT = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

let mockData = [
	{
		username: 'kaisertabuada',
		password: 'kaiser123'
	},
	{
		username: 'adminkaiser',
		password: 'admin123'
	},
	{
		username: 'trixie123',
		password: 'trix123'
	},
	{
		username: 'kresthianalouise',
		password: 'kv123'
	}
]

// ITEM 1
app.get("/home", (req, res) => res.send(`Welcome to the home page!`));
// ITEM 3
app.get("/users", (req, res) => res.send(mockData));
// ITEM 5
app.delete("/delete-users", (req, res) => {
	//console.log(req.body.username); 		// value check
	for(let i = 0; i < mockData.length; i++) {
		if(req.body.username === mockData[i].username) {
			mockData.splice(i, 1);
			i--;
			res.send(`User ${req.body.username} deleted!`)
			//console.log(mockData) 		// console log to check array contents
		}
	}
	
});

app.listen(PORT, () => console.log(`Server running fine at port ${PORT}...`));